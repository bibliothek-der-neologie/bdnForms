# bdnForms

> Generate HTML forms from RDF/Turtle

bdnForms is a JavaScript library to generate HTML forms from RDF/Turtle for the [Bibliothek der Neologie](https://bdn-edition.de) project. It was originally written by [Hannes Riebl](https://github.com/hriebl/) for the use with [TextGrid](http://textgrid.de) and is thus derived from [tgForms](https://github.com/hriebl/tgForms). By now, its sole use is as a dependency for [bdnDatabase](https://gitlab.gwdg.de/bibliothek-der-neologie/bdnDatabase).

## Installation

This is an npm package. Install it with `npm install bdnforms`.

## Vocabulary

bdnForms understands some [RDF Schema](http://www.w3.org/TR/rdf-schema) properties, namely rdfs:domain, rdfs:label, and rdfs:range, and interprets their subjects as form fields. Forms can be generated for classes that are used as objects of rdfs:domain. The library also comes with the following specific properties to refine forms:

### tgforms:hasInput

> Sets the input type for a property.

### tgforms:hasDefault

> Sets the default value for a property.

### tgforms:hasOption

> Sets a dropdown option for a property.

### tgforms:hasPriority

> Sets the priority for a property. Higher priorities appear first.

### tgforms:isRepeatable

> Makes a property repeatable.

The following input types are available for the use with `tgforms:hasInput`:

### tgforms:button

> A button. May be manually scripted.

### tgforms:checkbox

> A checkbox. Useful for boolean properties.

### tgforms:dropdown

> A dropbox menu. Useful if there is a limited number of options.

### tgforms:text

> A text field. Useful for short texts.

### tgforms:textarea

> A text area. Useful to longer texts.

------

[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg?style=flat)](https://github.com/semantic-release/semantic-release)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat)](http://commitizen.github.io/cz-cli/)